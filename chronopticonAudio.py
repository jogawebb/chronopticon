# Code for recording and processing audio for Chronopticon. 

import pyaudio
import wave

p = pyaudio.PyAudio()

chunk = 1024
wav_filename = 'test.wav'
resolution = pyaudio.paInt16

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORDING_TIME = 5

frames = []

stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=chunk)
wavfile = wave.open(wav_filename, 'wb')

def get_device(): 
    for i in range(p.get_device_count()): 
        print(p.get_device_info_by_index(i).get('name'))

get_device()


for i in range(0, int((RATE / chunk) * RECORDING_TIME)): 
    data = stream.read(chunk)
    frames.append(data)

stream.stop_stream()
wavfile.setnchannels(CHANNELS)
wavfile.setsampwidth(p.get_sample_size(resolution))
wavfile.setframerate(RATE)
wavfile.writeframes(b''.join(frames))
wavfile.close()