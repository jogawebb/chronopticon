# Code to run on the Chronopticon client machine. 

import socket
import datetime

BUFFER = 1048576

def main(): 
    host = '192.168.178.20'
    port = 8000
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    
    while True: 
        data = s.recv(BUFFER)
    
        if data[:3].decode() == 'new': 
            ok_message = 'ok'
            ok_message = ok_message.encode()
            s.send(ok_message)
            print('Sent ok.')
            
            data = s.recv(BUFFER)
        
            if data[:4].decode() == 'size': 
                fileSize = int(data[4:].decode())
                print('File size: ' + str(fileSize))
                ok_message = 'show'
                ok_message = ok_message.encode()
                s.send(ok_message)
                print('Asked for file.')
            
                byteData = s.recv(BUFFER)
                totalRecv = len(byteData)    
                f = open('new_' + str(datetime.datetime.now()) + '.jpg', 'wb')
                f.write(byteData)  
                
                while totalRecv < fileSize: 
                    data = s.recv(BUFFER)
                    totalRecv += len(data)
                    f.write(data)                      
                print('Download Complete')    
                f.close()
    
if __name__ == '__main__': 
    main()
    
    
