# Code for checking the rotary encoder and displaying an image in Chronopticon. 

# Import the relevant libraries. 
import glob
import os, sys
import pygame
import time
import timeit
import serial
from scipy.interpolate import interp1d
from pygame.locals import * 

DIMENSIONS = (1024, 768)
STEPS_PER_INCREMENT = 4

PATH_TO_IMAGES = '/home/pi/Desktop/whiteSketch/new_2019*.jpg'
PATH_TO_SOUNDS = '/home/pi/Desktop/whiteSketch/*combined.wav'
ARDUINO_LOCATION = '/dev/ttyACM0'
MINIMUM = -500

pygame.init()
pygame.mixer.init()
pygame.display.set_caption("Everything that's been here is here right now.")
display_surface = pygame.display.set_mode(DIMENSIONS, pygame.FULLSCREEN)
display_surface.fill((255, 255, 255))

serial_connection = serial.Serial(ARDUINO_LOCATION, 9600, timeout=0.2)

def render_image(index):
    list_of_images = glob.glob(PATH_TO_IMAGES)
    list_of_images.sort()
    number_of_images = len(list_of_images)
    list_of_sounds = glob.glob(PATH_TO_SOUNDS)
    list_of_sounds.sort()
    number_of_sounds = len(list_of_sounds)

    image_file_index = int((abs(index) / abs(MINIMUM)) * number_of_images)
    sound_file_index = int((abs(index) / abs(MINIMUM)) * number_of_sounds)

    print(image_file_index, sound_file_index)
    
    try:
        image_file = list_of_images[number_of_images - image_file_index - 1]
        sound_file = list_of_sounds[number_of_sounds - sound_file_index - 1]
    except IndexError:
        image_file = list_of_images[number_of_images - 1]
        sound_file = list_of_sounds[number_of_sounds - 1]
        
    the_image = pygame.image.load(image_file)
    the_image = pygame.transform.scale(the_image, (DIMENSIONS))

    display_surface.blit(the_image, (0, 0))
    pygame.display.update()
    
    pygame.mixer.music.load(sound_file)
    pygame.mixer.music.play(0)

def main():
    running = True
    last_index = None
    start_time = time.time()
    down_time = time.time()
    while running == True:
        line = serial_connection.readline()
        if not line:
            pass
        else:
            now_time = time.time()
            time_difference = now_time - start_time
            #print(time_difference)
            if time_difference > .75: 
                index = line.decode()
                index = index.rstrip('\n\r')
                index = int(index)
                print(index)
                start_time = time.time()

                if index != last_index: 
                    render_image(index)
                    last_index = index
                else:
                    at_rest = time.time()
                    if at_rest - down_time > 10:
                        index = 0
            else: 
                pass
        for event in pygame.event.get():
            if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                running = False
                pygame.display.quit()
            
main()
    