import os
import glob
import numpy as np
import datetime
from PIL import Image
import socket

# Set the hostname of the server machine. 
HOST = '192.168.178.20'
PORT = 8000
BUFFER = 1048576

# Declare global variables: 
PATH_TO_DIRECTORY = '/home/pi/Desktop/whiteSketch/*'
ITERATIONS = 6
MARGIN = 20
IMAGE_SIZE = (3456, 2304)
OPACITY = 65 # Supports values 0-255 (transparent to opaque)

def take_photo(): 
    # Run a shell script to take a picture with the camera. 
    command1 = "env LANG=C gphoto2 --port usb: --capture-image-and-download --folder /home/gabe/Desktop/"
    os.system(command1)
    
    # Returns the file name of the most recent photo. 
    list_of_files = glob.glob(PATH_TO_DIRECTORY)
    the_photo = max(list_of_files, key=os.path.getctime)
    
    # Returns the location of the newest file. 
    return(the_photo)
    
def get_pixel_data(path_to_photo):
    # Opens the photo from its filepath. 
    the_photo = Image.open(path_to_photo)
    the_photo = the_photo.convert('RGB')
    
    # Turns the photo into an one-dimensional array of floats. 
    pixel_data = np.asarray(the_photo)
    pixel_data = pixel_data.astype('float32')
    
    # Returns the array of pixel data. 
    return(pixel_data)

def compare_pixels(array1, array2, background): 

    # Make an image mask. 
    image_mask = Image.new('RGBA', IMAGE_SIZE)
    image_mask = np.asarray(image_mask, 'float32')
    original_shape = image_mask.shape

    # Uniformly reshape the image arrays. 
    array1 = np.reshape(array1, (-1, 3))
    array2 = np.reshape(array2, (-1, 3))
    background = np.reshape(background, (-1, 3))
    
    # Add another dimension to accommodate the alpha channel. 
    image_mask = np.reshape(image_mask, (-1, 4))

    # Calculates the differences in values between new image and b/g. 
    differences = abs(array2 - background)
    
    # Get the indices of where the pixels are different. 
    mask_coords = np.where(differences > MARGIN)
    mask_coords = mask_coords[0]
    
    # Overwrite the different pixels with transparent white. 
    for i in mask_coords: 
        image_mask[i] = [255, 255, 255, OPACITY]

    # Reshape the arrays and return the values. 
    image_mask = np.asarray(image_mask, 'uint8')
    image_mask = np.reshape(image_mask, original_shape) 
    return(image_mask)

def rebuild_image(old_array, new_mask):
    # Convert the old image for use as pixel data. 
    old_array = np.asarray(old_array, 'uint8')
    
    # Create the new pictures as PIL Image objects. 
    new_image = Image.fromarray(new_mask, mode='RGBA')
    background_image = Image.fromarray(old_array, 'RGB')

    # Layer the photos and save them. 
    background_image.paste(new_image, (0, 0), new_image)
    background_image.save('/home/pi/Desktop/whiteSketch/composite' + str(datetime.datetime.now()) + '.jpg', optimize=True, quality=75)
    
def send_photo(filename, connection): 
    # Send message to client that a new photo is ready. 
    new_file_message = 'new'
    new_file_message = new_file_message.encode()
    connection.send(new_file_message)
    
    # Wait for an ok from the client machine. 
    clientOK = connection.recv(BUFFER)
    
    if clientOK[:2].decode() == 'ok':
        # Send the file size. 
        fileSize = str(os.path.getsize(filename))
        fileSize = 'size' + fileSize
        fileSize = fileSize.encode()
        connection.send(fileSize)
        print('Sent file size.')
        
        clientOK = connection.recv(BUFFER)
        
        # Send the file. 
        if clientOK[:4].decode() == 'show': 
            with open(filename, 'rb') as f: 
                bytesToSend = f.read(BUFFER)
                connection.send(bytesToSend)
                    
                while bytesToSend != ''.encode(): 
                    bytesToSend = f.read(BUFFER)
                    connection.send(bytesToSend)
                    print('Sending data...')
                    
            f.close()

    else: 
        error_message = 'ERROR'
        error_message = error_message.encode()
        connection.send(error_message)

def main():

    # Start the server. 
    the_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    the_socket.bind((HOST, PORT))
    the_socket.listen(5)
    # Will wait for the client connection before launching main loop. 
    connection, address = the_socket.accept()
    print('Server started successfully.')
    
    # Take the first background photo. 
    i = 0
    background_photo = None
    
    # Takes a photo and sets it as the background. 
    while i < 1: 
        background_photo = take_photo()
        background_photo = get_pixel_data(background_photo)
        i += 1

    # Takes new photos, layers them over old ones. 
    while i < ITERATIONS: 

        print('Client successfully connected at ' + str(address))
    
        # Get the newest photo in the folder. 
        list_of_files = glob.glob(PATH_TO_DIRECTORY)
        first_photo = max(list_of_files, key=os.path.getctime)

        # Take the new picture. 
        second_photo = take_photo()

        # Get the pixel data for the photos and compare. 
        array1 = get_pixel_data(first_photo)
        array2 = get_pixel_data(second_photo)
        new_data = compare_pixels(array1, array2, background_photo)
        rebuild_image(array1, new_data)
        
        # Send the most recent photo. 
        list_of_files = glob.glob(PATH_TO_DIRECTORY)
        send_photo(max(list_of_files, key=os.path.getctime), connection)
        
        i += 1
        
    the_socket.close()
        
main()